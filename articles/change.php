<?php
  /* Принимаем данные из формы */
  $id = $_POST["id"];

  //print_r($_POST);

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <link rel="stylesheet" href="../js/bootstrap.min.js">
  <link rel="stylesheet" href="../css/style.css">
  <title>Article1</title>
  <style>
    .article__heading {
      font-size: 50px;
    }
    .exit-link {
      font-size: 30px;
      text-decoration: none;
      color: black;
      border: 2px solid black;
      border-radius: 5px;
    }
    .exit-link:hover {
      background-color: gray;
      color: black;
    }
    .form__heading {
      font-weight: 500;
      font-size: 24px;
    }
    .comments-block {
      border: 2px solid orange;
      padding: 10px;
      margin-bottom: 10px;
    }
    .comment__heading {
      color: orange;
    }
    .comment__date {
      font-size: 12px;
      opacity: 0.7;
    }
    .btn-cust {
      background-color: orange;
      border: 2px solid orange;
    }
    /*.change {
      padding: 7px 14px;
      text-decoration: none;
      color: white;
      background-color: orange;
      border-radius: 5px;
    }*/
    
  </style>
</head>

<body>
    <main>
      <div class="container mt-5 mb-5">
      <div class="row justify-content-center">
        <form class="col-4 text-center" name="comment" method="post" action="change-php.php">  
              <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label form__heading">Изменить комментарий</label>
                <textarea name="text_comment" class="form-control mb-3 form__input" placeholder="Введите комментарий здесь"></textarea>
                <input type="hidden" name="id" value="<?=$id?>" />
                <input type="hidden" name="page_id" value="150" />
                <button type="submit" class="btn btn-success btn-cust">Изменить</button>
              </div>  
          </form>
      </div>
      
    </div>
    </main>
</body>
</html>