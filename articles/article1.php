<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../js/bootstrap.min.js">
	<link rel="stylesheet" href="../css/style.css">
	<title>Article1</title>
	<style>
		.article__heading {
			font-size: 50px;
		}
		.exit-link {
			font-size: 30px;
			text-decoration: none;
			color: black;
			border: 2px solid black;
			border-radius: 5px;
		}
		.exit-link:hover {
			background-color: gray;
			color: black;
		}
		.form__heading {
			font-weight: 500;
			font-size: 24px;
		}
		.comments-block {
			border: 2px solid orange;
			padding: 10px;
			margin-bottom: 10px;
		}
		.comment__heading {
			color: orange;
		}
		.comment__date {
			font-size: 12px;
			opacity: 0.7;
		}
		.btn-cust {
			background-color: orange;
			border: 2px solid orange;
		}
		/*.change {
			padding: 7px 14px;
			text-decoration: none;
			color: white;
			background-color: orange;
			border-radius: 5px;
		}*/
		
	</style>
</head>
<body>
	<header class="container mt-4 mb-5">
		<div class="row">
			<div class="col-4 text-center ">
				<p>Добро пожаловать, <?=$_COOKIE['user']?></p>
			</div>
			<div class="col-6">
				
			</div>
			<div class="col-2">
				<a class="exit-link" href="../main.php">Назад</a>
			</div>
		</div>
	</header>
	<main>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-10 ">
					<h1 class="article__heading text-center mb-5">Название статьи</h1>
					<div class="row justify-content-center mb-5">

							<img class="col-12" src="../img/article1.png" style="border-radius: 50%;" alt="#" >
						
					</div>
					
					<p class="article__description">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Earum, provident soluta ab cum sed possimus totam inventore corrupti! Reiciendis accusantium corporis hic quo aspernatur quaerat quidem inventore rem, vitae ad, ab perferendis iure deleniti similique eum asperiores aperiam eligendi suscipit minima, excepturi quam. Minima sunt accusantium tempora dolor dolores, quod eaque a veniam eius eos blanditiis sequi dolore quisquam facilis fugiat animi odio, autem? Nobis dolores possimus nesciunt repellendus, quia accusamus suscipit necessitatibus, inventore. Eligendi, magnam harum optio, error cupiditate recusandae tempore ut molestias ullam asperiores iusto ipsam vero repellat sequi eaque voluptate nobis culpa sapiente id libero corporis voluptas.</p>
				</div>
			</div>
			
				

		</div>
		<div class="container mt-5 mb-5">
			<div class="row justify-content-center">
				<form class="col-4 text-center" name="comment" method="post" action="comment.php">	
							<div class="mb-3">
							  <label for="exampleInputEmail1" class="form-label form__heading">Оставить комментарий</label>
							  <textarea name="text_comment" class="form-control mb-3 form__input" placeholder="Введите комментарий здесь"></textarea>
							  <input type="hidden" name="name" value="<?=$_COOKIE['user']?>" />
							  <input type="hidden" name="page_id" value="150" />
							  <button type="submit" class="btn btn-success btn-cust">Отправить</button>
							</div>	
					</form>
			</div>
			
		</div>

		<div class="container">
			<div class="row justify-content-center">
				<div class="col-8">
					<!-- <h3 class="name">Name</h2>
					<p class="message">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Non consectetur esse labore, nam quidem! Sequi fugit illo porro exercitationem animi excepturi, assumenda labore debitis architecto vitae autem vel, voluptates, quas.</p> -->
					<?php
					  $page_id = 150;
					  $mysqli = new mysqli("std-mysql", "std_1727_cwork2", "091202TUjhRF", "std_1727_cwork2");// Подключается к базе данных
					  $result_set = $mysqli->query("SELECT * FROM `comments` WHERE `page_id`='$page_id'"); //Вытаскиваем все комментарии для данной страницы
					  while ($row = $result_set->fetch_assoc()) {
					   ?> 
					   		<div class="comments-block">
					   			<h1 class="comment__heading"> <?php echo $row["name"]; ?></h1>
					   			<p class="comment__text"><?php echo $row["text_comment"]; ?></p>
					   			<p class="comment__date"><?php echo $row["date"]; ?></p>

					   			<form action="change.php" method="post" class="change">
					   				<input type="hidden" name="id" value="<?php echo $row["id"]; ?>" />
					   				<button type="submit" class="btn btn-success btn-cust">Изменить</button>
					   			</form>
					   			<!-- <a href="change.php" class="change">Изменить</a> -->
					   		</div>
					   		


					   		 <?php
					  }
					?>
				</div>
			</div>
		</div>

	</main>
</body>
</html>