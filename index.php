<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="js/bootstrap.min.js">
	<title>cwork2</title>
</head>
<body>
	<div class="container mt-4">
		<?php 
			if (!isset($_COOKIE['user'])):
		?>
			<div class="row">
				<div class="col">
					<h2>Вход</h2>
					<form method="post" action="server/auth.php">
						    <input type="text" name="name" class="form-control" id="name" placeholder="Введите имя"><br>
						    <input type="password" name="password" class="form-control" id="password" placeholder="Введите пароль"><br>
						  <button type="submit" class="btn btn-success">Войти</button>
					</form>
				</div>
				<div class="col">
				    <h2>Регистрация</h2>
					<form method="post" action="check.php">
						    <input type="text" class="form-control" id="name" name="name" placeholder="Введите имя"><br>
						    <input type="password" name="password" class="form-control" id="password" placeholder="Введите пароль"><br>
						  <button type="submit" class="btn btn-success">Зарегистрироваться</button>
					</form>
				</div>
				<?php else: ?>
					<?php 
						require_once 'main.php';  
					?>
				<?php endif;?> 
			</div>
	</div>
</body>
</html>
